using System;

namespace Banco3Clientes
{
     class Cliente
     {
          private string nombre;
          private int monto;

          public Cliente(string nom)
          {
               nombre = nom;
               monto = 0;
          }

          public void Deposito(int m)
          {
               monto = monto + m;
          }

          public void Retiro(int m)
          {
               monto = monto - m;
          }

          public int RetornoMonto()
          {
               return monto;
          }

          public void Imprimir()
          {
               Console.WriteLine(nombre + " tiene depositado la suma de: " + monto);
          }
     }

     class Banco
     {
          private Cliente cliente1, cliente2, cliente3;

          public Banco()
          {
             
              cliente1 = new Cliente("Wagner");
              cliente2 = new Cliente("Rafael");
              cliente3 = new Cliente("Miguel");
          }

          public void Operar()
          {
              int ingreso1, ingreso2, ingreso3;
              Console.WriteLine("Ingrese el valor a depositar: ");
              ingreso1 = int.Parse(Console.ReadLine());
              cliente1.Deposito(ingreso1);
              Console.WriteLine("Ingrese el valor a depositar: ");
              ingreso2 = int.Parse(Console.ReadLine());
              cliente2.Deposito(ingreso2);
              Console.WriteLine("Ingrese el valor a depositar: ");
              ingreso3 = int.Parse(Console.ReadLine());
              cliente3.Deposito(ingreso3);
          }

          public void DepositosTotales()
          {
               int t = cliente1.RetornoMonto() +
                       cliente2.RetornoMonto() +
                       cliente3.RetornoMonto();
               Console.WriteLine("El total de dinero en el banco es:" + t);
               cliente1.Imprimir();
               cliente2.Imprimir();
               cliente3.Imprimir();
          }

          static void Main(string[] args)
          {
               Banco banco1 = new Banco();
               banco1.Operar();
               banco1.DepositosTotales();
               Console.ReadKey();
          }
     }
}
